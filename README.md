# GitLab CI template for great_expectations

This project implements a generic GitLab CI template for running [great_expectations](https://link.to.tool.com/)  automated tests.

It provides several features, usable in different modes (by configuration).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/great_expectations'
    ref: '1.0.0'
    file: '/templates/gitlab-ci-great_expectations.yml'
```

:warning: depending on your needs and environment, you might have to use [one of the template variants](#variants).

## `great_expectations` job

This job starts [great_expectations](https://link.to.tool.com/) tests.

It uses the following variable:

| Name                  | description                               | default value     |
| --------------------- | ----------------------------------------- | ----------------- |
| `GE_IMAGE`           | The Docker image used to run great_expectations          | `greatexpectations/great_expectations:python-3.7-buster-ge-0.12.0`      |
| `GE_ROOT_DIR`        | The root great_expectations project directory            | `.`               |
| `GE_EXTRA_ARGS`      | great_expectations extra [options](link-to-ref-doc-here) | _none_            |
| `REVIEW_ENABLED`      | Set to enable great_expectations tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled) |

### Unit tests report integration

great_expectations test reports are [integrated to GitLab by generating JUnit reports](https://docs.gitlab.com/ee/ci/junit_test_reports.html).

This is done using the following CLI options: `--junit --output=reports/`  

### Base URL auto evaluation

By default, the great_expectations template tries to auto-evaluate the base URL (i.e. the variable pointing at server under test) by
looking either for a `$environment_url` variable or for an `environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv) variable `$environment_url`
or through a basic `environment_url.txt` file, then the great_expectations test will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your great_expectations tests.

If you're not using a smart deployment job, you may still explicitly declare the `GE_BASE_URL` variable (but that
will be unfortunately hardcoded to a single server).
